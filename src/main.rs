#[macro_use]
extern crate serde_derive;
extern crate docopt;
extern crate bcrypt;

use bcrypt::{DEFAULT_COST, hash};
use docopt::Docopt;

const VERSION: &'static str = env!("CARGO_PKG_VERSION");
const USAGE: &'static str = "
Command line tool for password hashing using bcrypt.

Usage:
  bcryptonator <passwords>...
  bcryptonator --cost=<cost> <passwords>...
  bcryptonator (-h | --help)
  bcryptonator --version

Options:
	-h --help			     	Show this screen.
	--version			     	Show version.
	-c --cost <value>		   	Cost of hashing between 4 and 31 [default: 12].
";

#[derive(Debug, Deserialize)]
struct Args {
    flag_cost: isize,
    arg_passwords: Vec<String>,
    flag_version: bool
}


fn main() {
	let args: Args = Docopt::new(USAGE)
                            .and_then(|d| d.deserialize())
                            .unwrap_or_else(|e| e.exit());
    match args.flag_version {
    	true => println!("Version {}", VERSION),
    	false => encrypt(args.arg_passwords, args.flag_cost as u32)
    }

fn encrypt(passwords: Vec<String>, cost: u32){ 
	let mut iter = cost;
	if iter == 0{
		iter = DEFAULT_COST;
	}
	for password in passwords {
		let hash = hash(&password, iter);
		println!("{} \t\t {}", password, hash.unwrap().as_str());
	}
}	
}
